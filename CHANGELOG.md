## 0.3.3

- feat: Upgraded `freezed` and `freezed_annotation` packages to `2.x.x`.

## 0.3.2

- fix: Fixed promise for nullable types.

## 0.3.1

- fix: Handling snapshot errors.

## 0.3.0

- fix!: Passing T to all variants and AsyncSnapshot in fromSnapshot constructor.

## 0.2.0

- refactor: Renamed `AsyncValue`, `AsyncLoading`, `AsyncData` and `AsyncError`
  types to `Promise`, `PromiseLoading`, `PromiseData` and `PromiseError` to
  avoid name clash with `riverpod`.
- feat: Added `Promise.value` getter.

## 0.1.1

- fix: Passing `T` type to `AsyncData`, so it's no always of type `dynamic`.

## 0.1.0

- Added `AsyncValue` type.
- Added `useFutureValue` hook.
- Added `useStreamValue` hook.
