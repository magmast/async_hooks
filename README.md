# async_hooks

`useFuturePromise` and `useStreamPromise` hooks returning `Promise` type
inspired by `riverpod`s `AsyncValue`.

## THIS PACKAGE IS STILL IN DEVELOPMENT

Currently 0.0.x part is incremented for fixes and new features and 0.x.0 part is
incremented for releases with breaking changes. This package will follow
semantic versioning after 1.0.0 release.
