library async_utils;

import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'async_hooks.freezed.dart';

@freezed
class Promise<T> with _$Promise<T> {
  const Promise._();

  const factory Promise.loading() = PromiseLoading<T>;
  const factory Promise.error(Object? error) = PromiseError<T>;
  const factory Promise.data(
    T value, {
    @Default(true) bool isDone,
  }) = PromiseData<T>;

  factory Promise.fromSnapshot(AsyncSnapshot<T> snapshot) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
      case ConnectionState.waiting:
        return const Promise.loading();
      case ConnectionState.active:
        return snapshot.hasError
            ? Promise.error(snapshot.error)
            : Promise.data(snapshot.data as T, isDone: false);
      case ConnectionState.done:
        return snapshot.hasError
            ? Promise.error(snapshot.error)
            : Promise.data(snapshot.data as T);
    }
  }

  T? get value => whenOrNull(
        data: (value, _) => value,
      );
}

Promise<T> useFuturePromise<T>(Future<T> future) {
  final snapshot = useFuture(future);
  return Promise.fromSnapshot(snapshot);
}

Promise<T> useStreamPromise<T>(Stream<T> stream) {
  final snapshot = useStream(stream);
  return Promise.fromSnapshot(snapshot);
}
